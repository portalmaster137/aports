# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php82-pecl-mongodb
_extname=mongodb
pkgver=1.19.2
pkgrel=0
pkgdesc="PHP 8.2 MongoDB driver - PECL"
url="https://pecl.php.net/package/mongodb"
arch="all"
license="Apache-2.0"
_phpv=82
_php=php$_phpv
depends="$_php-common"
makedepends="cyrus-sasl-dev icu-dev openssl-dev>3 $_php-dev snappy-dev zstd-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir"/$_extname-$pkgver
provides="$_php-mongodb=$pkgver-r$pkgrel" # for backward compatibility
replaces="$_php-mongodb" # for backward compatibility

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=/usr/bin/php-config$_phpv
	make
}

check() {
	# tests requires additional dependencies (vagrant)
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
cac8429cddda524d2f073e4dc7e73cc5a3935881522553caa4148baec16431ba01465d11bac0bd555bfb17794a888d5d3ee4e5dbac9b832ea86a19d0e58b97bd  php-pecl-mongodb-1.19.2.tgz
"
